-- $Id$
--
-- This file is part of pgets.
--
-- pgets is free software; you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free
-- Software Foundation; either version 2 of the License, or (at your
-- option) any later version.
--
-- pgets is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with pgets; if not, write to the Free Software Foundation,
-- Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

-- Create database

DROP DATABASE pgets;
CREATE DATABASE pgets;

\c pgets

-- Create tables

\i pgets.sql

-- Manage access rights

-- A user for the transfer program with write access to the table
DROP USER pgets_fill;
CREATE USER pgets_fill PASSWORD 'mahatma';
GRANT INSERT ON pgets_accounting TO pgets_fill;

-- A user for the web frontend with read access to the table
DROP USER pgets_web;
CREATE USER pgets_web PASSWORD 'gandhi';
GRANT SELECT ON pgets_accounting TO pgets_web;
