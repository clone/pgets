# -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

# $Id$

# This file is part of pgets.
#
# pgets is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pgets is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pgets; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

AC_PREREQ(2.59)
AC_INIT([pgets],[0.1],[mzctrgf (at) 0pointer (dot) de])
AC_CONFIG_SRCDIR([src/pgets.c])
AC_CONFIG_HEADERS([config.h])
AM_INIT_AUTOMAKE([foreign -Wall])

AC_SUBST(PACKAGE_URL, [http://0pointer.de/lennart/projects/pgets/])

if type -p stow > /dev/null && test -d /usr/local/stow ; then
   AC_MSG_NOTICE([*** Found /usr/local/stow: default install prefix set to /usr/local/stow/${PACKAGE_NAME}-${PACKAGE_VERSION} ***])
   ac_default_prefix="/usr/local/stow/${PACKAGE_NAME}-${PACKAGE_VERSION}"
fi

# Checks for programs.
AC_PROG_CC
AC_PROG_LN_S
AC_PROG_MAKE_SET

# If using GCC specifiy some additional parameters
if test "x$GCC" = "xyes" ; then
   CFLAGS="$CFLAGS -pipe -Wall"
fi

# Check for PostgreSQL
AC_ARG_ENABLE(postgres, AS_HELP_STRING(--disable-postgres,Don't build postgres client),
[case "${enableval}" in
    yes) postgres=yes ;;
    no) postgres=no ;;
    *) AC_MSG_ERROR(bad value ${enableval} for --disable-postgres) ;;
esac],[postgres=yes])

if test "x$postgres" = "xyes" ; then
    AC_CHECK_LIB([pq], [PQconnectdb],,[AC_MSG_ERROR([*** Sorry, you have to install libpq or use --disable-postgres ***])])
    AC_CHECK_HEADER([postgresql/libpq-fe.h],,[AC_MSG_ERROR([*** Sorry, you have to install postgresql-dev or use --disable-postgres ***])])
fi


AM_CONDITIONAL([POSTGRES], [test "x$postgres" = "xyes"])

# Check for SQLite
AC_ARG_ENABLE(sqlite, AS_HELP_STRING(--disable-sqlite,Don't build sqlite client),
[case "${enableval}" in
    yes) sqlite=yes ;;
    no) sqlite=no ;;
    *) AC_MSG_ERROR(bad value ${enableval} for --sqlite-postgres);;
esac],[sqlite=yes])

if test "x$sqlite" = "xyes" ; then
    AC_CHECK_LIB([sqlite], [sqlite_open],,[AC_MSG_ERROR([*** Sorry, you have to install libsqlite or use --disable-sqlite ***])])
    AC_CHECK_HEADER([sqlite.h],,[AC_MSG_ERROR([*** Sorry, you have to install libsqlite-dev or use --disable-sqlite ***])])
fi

AM_CONDITIONAL([SQLITE], [test "x$sqlite" = "xyes"])

# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([fcntl.h inttypes.h limits.h stdlib.h string.h termios.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_MODE_T
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_STRUCT_TM
AC_TYPE_UID_T
AC_C_VOLATILE

# Checks for library functions.
AC_TYPE_SIGNAL
AC_FUNC_STAT
AC_CHECK_FUNCS([localtime_r memset strerror strndup])

# LYNX documentation generation
AC_ARG_ENABLE(lynx,
        AS_HELP_STRING(--disable-lynx,Turn off lynx usage for documentation generation),
[case "${enableval}" in
  yes) lynx=yes ;;
  no)  lynx=no ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --disable-lynx) ;;
esac],[lynx=yes])

if test x$lynx = xyes ; then
   AC_CHECK_PROG(have_lynx, lynx, yes, no)

   if test x$have_lynx = xno ; then
     AC_MSG_ERROR([*** Sorry, you have to install lynx or use --disable-lynx ***])
   fi
fi

AM_CONDITIONAL([USE_LYNX], [test "x$lynx" = xyes])

AC_CONFIG_FILES([src/Makefile Makefile doc/Makefile doc/README.html php/Makefile sql/Makefile])
AC_OUTPUT
