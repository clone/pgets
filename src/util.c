/* $Id$
 *
 * This file is part of pgets. 
 *
 * pgets is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * pgets is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pgets; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <unistd.h>
#include <termios.h>
#include <string.h>

#include "util.h"

ssize_t loop_read (int FILEDES, void *BUFFER, size_t SIZE) {
    ssize_t c = 0;
    
    while (SIZE > 0) {
        ssize_t r = read(FILEDES, BUFFER, SIZE);

        if (r <= 0) {
            if (c == 0)
                return r;
            else
                return c;
        }

        SIZE -= r;
        c += r;
        BUFFER += r;
    }

    return c;
}

ssize_t loop_write(int FILEDES, const void *BUFFER, size_t SIZE) {
    ssize_t c = 0;
    
    while (SIZE > 0) {
        ssize_t r = write(FILEDES, BUFFER, SIZE);

        if (r <= 0) {
            if (c == 0)
                return r;
            else
                return c;
        }

        SIZE -= r;
        c += r;
        BUFFER += r;
    }

    return c;
}

// The name says it all.
void flush_data(int fd) {
    tcflush(fd, TCIFLUSH);
    tcflush(fd, TCIFLUSH);
}

ssize_t read_line(int FILEDES, char *ln, size_t SIZE) {
    ssize_t c = 0;

    while (SIZE > 0) {
        ssize_t r;
        
        if ((r = read(FILEDES, ln, 1)) != 1) {
            if (r < 0 && !c)
                return -1;

            break;
        }

        SIZE--;
        c++;

        if (*ln == '\r' || *ln == '\n') {
            ln++;
            break;
        }

        ln++;
    }

    if (SIZE)
        *ln = 0;

    return c;
}
