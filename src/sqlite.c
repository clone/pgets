/* $Id$
 *
 * This file is part of pgets. 
 *
 * pgets is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * pgets is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pgets; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <time.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sqlite.h>

#include "db.h"


static int busy(void *v, const char *name, int try) {
    fprintf(stderr, "Table '%s' locked (try #%i), sleeping 2s ...           \r", name, try);
    sleep(2);
    return 0;
}

void* db_connect(const char*t) {
    sqlite *db;
    char *e;

    if (!t) {
        fprintf(stderr, "Database specification required.\n");
        return NULL;
    }

    if (!(db = sqlite_open(t, 0, &e))) {
        fprintf(stderr, "Failed to open database: %s\n", e);
        free(e);
    }

    sqlite_busy_handler(db, busy, NULL);
    
    return db;
}

void db_disconnect(void *db) {
    assert(db);
    sqlite_close((sqlite*) db);
}

int db_write_entry(void *vdb, const struct entry *entry) {
    static time_t t = 0;
    char query[512];
    struct tm tm;
    sqlite *db = vdb;
    int year;
    char *e = NULL;
    int ret;

    assert(db);

    if (t == 0) {
        t = time(NULL);
        localtime_r(&t, &tm);
    }

    if (entry->month < tm.tm_mon+1 || (entry->month == tm.tm_mon+1 && entry->day <= tm.tm_mday))
        year = 1900+tm.tm_year;
    else
        year = 1900+tm.tm_year-1;
    
    snprintf(query, sizeof(query), "INSERT INTO pgets_accounting (remote_msn, local_mm, participant, incoming, _timestamp, duration) VALUES ('%s', %i, %i, '%c', '%04i-%02i-%02i %02i:%02i:00', %i)",
             entry->remote_msn, entry->local_mm, entry->participant, entry->incoming ? 't' : 'f', year, entry->month, entry->day, entry->hour, entry->minute, entry->duration);

    if ((ret = sqlite_exec(db, query, NULL, NULL, &e)) != SQLITE_OK) {

        if (ret == SQLITE_CONSTRAINT) {
            free(e);
            return 1;
        }
        
        fprintf(stderr, "sqlite_exec(): %s\n", e);
        free(e);
        return -1;
    }
    
    return 0;
}
