/* $Id$
 *
 * This file is part of pgets. 
 *
 * pgets is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * pgets is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pgets; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h> 

#include "modem.h"

static struct termios _saved_termios;

/* Opens the modem and sets the baud rate */
int modem_open(const char *dev) {
    struct termios pts; 
    int fd, n;

    if ((fd = open(dev, O_RDWR|O_NDELAY)) < 0) {
        perror("Serial port open failure");
        return -1;
    }

    n = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, n & ~O_NDELAY);

    if (tcgetattr(fd, &_saved_termios) != 0) {
        perror("Serial port TERMIOS get failure");
        close(fd);
        return -1;
    }
   
    memset(&pts, 0, sizeof pts);
    pts.c_cflag = CS8 | CLOCAL | CREAD;
    pts.c_iflag = IGNPAR | IGNBRK | IGNCR | IXON | IXOFF;
    pts.c_oflag = 0;
    pts.c_lflag = 0;

    pts.c_cc[VMIN] = 1;
    pts.c_cc[VTIME] = 0;

    cfsetospeed(&pts, B9600);
    cfsetispeed(&pts, B9600);

    tcflush(fd, TCIFLUSH);
    if (tcsetattr(fd, TCSANOW, &pts) != 0) {
        perror("Serial port TERMIOS set failure");
        close(fd);
        return -1;
    }

    return fd;
}

/* Closes the modem device and resets the baudrate */
void modem_close(int fd) {
    tcflush(fd, TCIFLUSH);
    tcsetattr(fd, TCSANOW, &_saved_termios);
    close(fd);
}
