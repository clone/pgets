/* $Id$
 *
 * This file is part of pgets. 
 *
 * pgets is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * pgets is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pgets; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <time.h>
#include <assert.h>
#include <stdio.h>

#include <postgresql/libpq-fe.h>

#include "db.h"

void* db_connect(const char*t) {
    PGconn *pg;

    if (!t) {
        fprintf(stderr, "Database specification required.\n");
        return NULL;
    }

    if (!(pg = PQconnectdb(t)))
        return NULL;

    if (PQstatus(pg) != CONNECTION_OK) {
        fprintf(stderr, "Could not connect to database: %s\n", PQerrorMessage(pg));
        PQfinish(pg);
        return NULL;
    }

    return pg;
}

void db_disconnect(void *db) {
    assert(db);
    PQfinish((PGconn*) db);
}

int db_write_entry(void *db, const struct entry *entry) {
    static time_t t = 0;
    char query[512];
    struct tm tm;
    PGresult* r;
    PGconn *pg = db;
    int year;

    assert(pg);
    
    if (t == 0) {
        t = time(NULL);
        localtime_r(&t, &tm);
    }

    if (entry->month < tm.tm_mon+1 || (entry->month == tm.tm_mon+1 && entry->day <= tm.tm_mday))
        year = 1900+tm.tm_year;
    else
        year = 1900+tm.tm_year-1;
    
    snprintf(query, sizeof(query), "INSERT INTO pgets_accounting (remote_msn, local_mm, participant, incoming, _timestamp, duration) VALUES ('%s', %i, %i, '%c', TIMESTAMP '%04i-%02i-%02i %02i:%02i:00', %i)",
             entry->remote_msn, entry->local_mm, entry->participant, entry->incoming ? 't' : 'f', year, entry->month, entry->day, entry->hour, entry->minute, entry->duration);

    if (!(r = PQexec(pg, query)) || PQstatus(pg) != CONNECTION_OK || PQresultStatus(r) != PGRES_COMMAND_OK) {
        if (r) {
            if (PQresultStatus(r) == PGRES_FATAL_ERROR) {
                PQclear(r);
                return 1;
            }
                        
            fprintf(stderr, "Query [%s] failed (#1), reason given: %s\n", query, PQresultErrorMessage(r));
            PQclear(r);
        } else
            fprintf(stderr, "Query [%s] failed (#2), reason given: %s\n", query, PQerrorMessage(pg));

        return -1;
    }

    PQclear(r);
    return 0;
}
